# Korean translation for gnome-calendar.
# Copyright (C) 2013 gnome-calendar's contributors.
# This file is distributed under the same license as the gnome-calendar package.
# Hyunwoo Park <ez.amiryo@gmail.com>, 2018.
# Seong-ho Cho <shcho@gnome.org>, 2013, 2015-2023.
#
# -*- 참고 -*-
# 하기 사이트의 공식 표기에 따라 "Microsoft Teams"를 "마이크로소프트 팀즈"로 음역합니다.
# https://www.youtube.com/watch?v=66rrnoMdwp4
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-calendar master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-calendar/issues\n"
"POT-Creation-Date: 2023-03-04 13:46+0000\n"
"PO-Revision-Date: 2023-03-04 23:49+0900\n"
"Last-Translator: Seong-ho Cho <shcho@gnome.org>\n"
"Language-Team: GNOME Korea <gnome-kr@googlegroups.com>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Poedit 2.3.1\n"

#: data/appdata/org.gnome.Calendar.appdata.xml.in.in:7
#: data/org.gnome.Calendar.desktop.in.in:3 src/main.c:35
#: src/gui/gcal-application.c:178 src/gui/gcal-quick-add-popover.ui:135
#: src/gui/gcal-window.ui:4
msgid "Calendar"
msgstr "달력"

#: data/appdata/org.gnome.Calendar.appdata.xml.in.in:8
msgid "Calendar for GNOME"
msgstr "그놈용 달력 메모"

#: data/appdata/org.gnome.Calendar.appdata.xml.in.in:11
msgid ""
"GNOME Calendar is a simple and beautiful calendar application designed to "
"perfectly fit the GNOME desktop. By reusing the components which the GNOME "
"desktop is built on, Calendar nicely integrates with the GNOME ecosystem."
msgstr ""
"그놈 달력은 그놈 데스크톱에 완벽하게 맞춰 설계한 단순하고 아름다운 달력 프로"
"그램입니다. 그놈 달력은 그놈 데스크톱에 내장한 구성 요소를 재사용하여 그놈 환"
"경에 멋지게 어우러집니다."

#: data/appdata/org.gnome.Calendar.appdata.xml.in.in:16
msgid ""
"We aim to find the perfect balance between nicely crafted features and user-"
"centred usability. No excess, nothing missing. You’ll feel comfortable using "
"Calendar, like you’ve been using it for ages!"
msgstr ""
"멋지게 다듬은 기능과 사용자 중심의 활용성 간의 완벽한 균형의 유지가 목표입니"
"다. 잘나지도 부족하지도 않습니다. 오랫동안 달력을 사용해보셨던 것 같은 편안함"
"을 느끼실 수 있습니다!"

#: data/appdata/org.gnome.Calendar.appdata.xml.in.in:24
msgid "Week view"
msgstr "주별 보기"

#: data/appdata/org.gnome.Calendar.appdata.xml.in.in:28
msgid "Month view"
msgstr "월 단위 보기"

#: data/appdata/org.gnome.Calendar.appdata.xml.in.in:32
msgid "Event editor"
msgstr "행사 편집기"

#: data/org.gnome.Calendar.desktop.in.in:4
msgid "Access and manage your calendars"
msgstr "달력에 접근하고 관리합니다"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.Calendar.desktop.in.in:13
msgid "Calendar;Event;Reminder;"
msgstr "Calendar;달력;캘린더;Event;행사;이벤트;Reminder;알림;"

#: data/org.gnome.calendar.gschema.xml.in:6
msgid "Window maximized"
msgstr "창 최대화"

#: data/org.gnome.calendar.gschema.xml.in:7
msgid "Window maximized state"
msgstr "창 최대화 상태"

#: data/org.gnome.calendar.gschema.xml.in:11
msgid "Window size"
msgstr "창 크기"

#: data/org.gnome.calendar.gschema.xml.in:12
msgid "Window size (width and height)."
msgstr "창 크기(너비 및 높이)."

#: data/org.gnome.calendar.gschema.xml.in:16
msgid "Window position"
msgstr "창 위치"

#: data/org.gnome.calendar.gschema.xml.in:17
msgid "Window position (x and y)."
msgstr "창 위치(가로 및 세로)."

#: data/org.gnome.calendar.gschema.xml.in:21
msgid "Type of the active view"
msgstr "활성 보기 형식"

#: data/org.gnome.calendar.gschema.xml.in:22
msgid "Type of the active window view, default value is: monthly view"
msgstr "활성 창 보기의 형식입니다. 기본 값은 월별 보기 입니다"

#: data/org.gnome.calendar.gschema.xml.in:26
msgid "Weather Service Configuration"
msgstr "날씨 서비스 설정"

#: data/org.gnome.calendar.gschema.xml.in:27
msgid ""
"Whether weather reports are shown, automatic locations are used and a "
"location-name"
msgstr ""
"날씨 정보를 표시 여부에 따라 자동 위치 정보를 location-name과 함께 사용합니다"

#. Translators: %1$s is the start date and %2$s is the end date.
#. Translators: %1$s is the start date, and %2$s. For example: June 21 - November 29, 2022
#: src/core/gcal-event.c:1906 src/gui/gcal-event-popover.c:395
#, c-format
msgid "%1$s — %2$s"
msgstr "%1$s — %2$s"

#.
#. * Translators: %1$s is the start date, %2$s is the start time,
#. * %3$s is the end date, and %4$s is the end time.
#.
#: src/core/gcal-event.c:1914
#, c-format
msgid "%1$s %2$s — %3$s %4$s"
msgstr "%1$s, %2$s – %3$s %4$s"

#. Translators: %1$s is a date, %2$s is the start hour, and %3$s is the end hour
#. Translators: %1$s is the event name, %2$s is the start hour, and %3$s is the end hour
#: src/core/gcal-event.c:1930 src/gui/gcal-quick-add-popover.c:461
#, c-format
msgid "%1$s, %2$s – %3$s"
msgstr "%1$s, %2$s – %3$s"

#: src/gui/calendar-management/gcal-calendar-management-dialog.ui:4
msgid "Calendar Settings"
msgstr "달력 설정"

#: src/gui/calendar-management/gcal-calendars-page.c:354
msgid "Manage Calendars"
msgstr "달력을 관리합니다"

#. Update notification label
#: src/gui/calendar-management/gcal-calendars-page.c:384
#, c-format
msgid "Calendar <b>%s</b> removed"
msgstr "<b>%s</b> 달력을 제거했습니다"

#: src/gui/calendar-management/gcal-calendars-page.ui:24
msgid "Undo"
msgstr "실행 취소"

#: src/gui/calendar-management/gcal-calendars-page.ui:78
msgid "Add Calendar…"
msgstr "달력 추가…"

#: src/gui/calendar-management/gcal-edit-calendar-page.ui:19
msgid "Account"
msgstr "계정"

#: src/gui/calendar-management/gcal-edit-calendar-page.ui:46
msgid "Settings"
msgstr "설정"

#: src/gui/calendar-management/gcal-edit-calendar-page.ui:57
#: src/gui/event-editor/gcal-summary-section.ui:23
#: src/gui/gcal-event-popover.ui:111
#: src/gui/importer/gcal-import-file-row.c:151
msgid "Location"
msgstr "위치"

#: src/gui/calendar-management/gcal-edit-calendar-page.ui:84
msgid "Calendar name"
msgstr "달력 이름"

#: src/gui/calendar-management/gcal-edit-calendar-page.ui:109
#: src/gui/calendar-management/gcal-new-calendar-page.ui:33
msgid "Color"
msgstr "색상"

#: src/gui/calendar-management/gcal-edit-calendar-page.ui:134
msgid "Display calendar"
msgstr "달력 표시"

#: src/gui/calendar-management/gcal-edit-calendar-page.ui:146
msgid "Add new events to this calendar by default"
msgstr "새 행사를 이 달력에 기본으로 추가"

#: src/gui/calendar-management/gcal-edit-calendar-page.ui:158
msgid "Remove Calendar"
msgstr "달력 제거"

#: src/gui/calendar-management/gcal-file-chooser-button.c:49
msgid "Select a file"
msgstr "파일 선택"

#: src/gui/calendar-management/gcal-file-chooser-button.c:94
#: src/gui/calendar-management/gcal-new-calendar-page.ui:188
#: src/gui/calendar-management/gcal-new-calendar-page.ui:290
#: src/gui/event-editor/gcal-event-editor-dialog.ui:15
msgid "Cancel"
msgstr "취소"

#: src/gui/calendar-management/gcal-file-chooser-button.c:96
msgid "Open"
msgstr "열기"

#: src/gui/calendar-management/gcal-new-calendar-page.c:517
msgid "New Calendar"
msgstr "새 달력"

#: src/gui/calendar-management/gcal-new-calendar-page.c:695
msgid "Calendar files"
msgstr "달력 파일"

#: src/gui/calendar-management/gcal-new-calendar-page.ui:16
#: src/gui/calendar-management/gcal-new-calendar-page.ui:22
msgid "Calendar Name"
msgstr "달력 이름"

#: src/gui/calendar-management/gcal-new-calendar-page.ui:55
msgid "Import a Calendar"
msgstr "달력 가져오기"

#: src/gui/calendar-management/gcal-new-calendar-page.ui:72
msgid ""
"Alternatively, enter the web address of an online calendar you want to "
"import, or open a supported calendar file."
msgstr ""
"대신, 가져올 온라인 달력 웹 주소를 입력하거나 지원 달력 파일을 여십시오."

#: src/gui/calendar-management/gcal-new-calendar-page.ui:100
msgid "Open a File"
msgstr "파일 열기"

#: src/gui/calendar-management/gcal-new-calendar-page.ui:124
msgid "Calendars"
msgstr "달력"

#: src/gui/calendar-management/gcal-new-calendar-page.ui:148
msgid ""
"If the calendar belongs to one of your online accounts, you can add it "
"through the <a href=\"GOA\">online account settings</a>."
msgstr ""
"추가할 달력의 주소를 입력하십시오. 달력이 온라인 계정에 들어있다면, <a "
"href=\"GOA\">온라인 계정 설정</a>에서 추가할 수 있습니다."

#: src/gui/calendar-management/gcal-new-calendar-page.ui:175
msgid "Credentials"
msgstr "인증 정보"

#: src/gui/calendar-management/gcal-new-calendar-page.ui:196
msgid "Connect"
msgstr "연결"

#: src/gui/calendar-management/gcal-new-calendar-page.ui:225
msgid "User"
msgstr "사용자"

#: src/gui/calendar-management/gcal-new-calendar-page.ui:238
msgid "Password"
msgstr "암호"

#: src/gui/calendar-management/gcal-new-calendar-page.ui:294
#: src/gui/gcal-quick-add-popover.ui:98
msgid "Add"
msgstr "추가"

#: src/gui/event-editor/gcal-alarm-row.c:84
#, c-format
msgid "%1$u day, %2$u hour, and %3$u minute before"
msgid_plural "%1$u day, %2$u hour, and %3$u minutes before"
msgstr[0] "%1$u일, %2$u시간, %3$u분 전"

#: src/gui/event-editor/gcal-alarm-row.c:88
#, c-format
msgid "%1$u day, %2$u hours, and %3$u minute before"
msgid_plural "%1$u day, %2$u hours, and %3$u minutes before"
msgstr[0] "%1$u일, %2$u시간, %3$u분 전"

#: src/gui/event-editor/gcal-alarm-row.c:94
#, c-format
msgid "%1$u days, %2$u hour, and %3$u minute before"
msgid_plural "%1$u days, %2$u hour, and %3$u minutes before"
msgstr[0] "%1$u일, %2$u시간, %3$u분 전"

#: src/gui/event-editor/gcal-alarm-row.c:98
#, c-format
msgid "%1$u days, %2$u hours, and %3$u minute before"
msgid_plural "%1$u days, %2$u hours, and %3$u minutes before"
msgstr[0] "%1$u일, %2$u시간, %3$u분 전"

#: src/gui/event-editor/gcal-alarm-row.c:113
#, c-format
msgid "%1$u day and %2$u hour before"
msgid_plural "%1$u day and %2$u hours before"
msgstr[0] "%1$u일, %2$u시간 전"

#: src/gui/event-editor/gcal-alarm-row.c:117
#, c-format
msgid "%1$u days and %2$u hour before"
msgid_plural "%1$u days and %2$u hours before"
msgstr[0] "%1$u일, %2$u시간 전"

#: src/gui/event-editor/gcal-alarm-row.c:133
#, c-format
msgid "%1$u day and %2$u minute before"
msgid_plural "%1$u day and %2$u minutes before"
msgstr[0] "%1$u일, %2$u분 전"

#: src/gui/event-editor/gcal-alarm-row.c:137
#, c-format
msgid "%1$u days and %2$u minute before"
msgid_plural "%1$u days and %2$u minutes before"
msgstr[0] "%1$u일, %2$u분 전"

#: src/gui/event-editor/gcal-alarm-row.c:147
#, c-format
msgid "%1$u day before"
msgid_plural "%1$u days before"
msgstr[0] "%1$u일 전"

#: src/gui/event-editor/gcal-alarm-row.c:165
#, c-format
msgid "%1$u hour and %2$u minute before"
msgid_plural "%1$u hour and %2$u minutes before"
msgstr[0] "%1$u시간, %2$u분 전"

#: src/gui/event-editor/gcal-alarm-row.c:169
#, c-format
msgid "%1$u hours and %2$u minute before"
msgid_plural "%1$u hours and %2$u minutes before"
msgstr[0] "%1$u시간, %2$u분 전"

#: src/gui/event-editor/gcal-alarm-row.c:179
#, c-format
msgid "%1$u hour before"
msgid_plural "%1$u hours before"
msgstr[0] "%1$u시간 전"

#: src/gui/event-editor/gcal-alarm-row.c:191
#, c-format
msgid "%1$u minute before"
msgid_plural "%1$u minutes before"
msgstr[0] "%1$u분 전"

#: src/gui/event-editor/gcal-alarm-row.c:198
msgid "Event start time"
msgstr "행사 시작 시간"

#: src/gui/event-editor/gcal-alarm-row.ui:11
msgid "Toggles the sound of the alarm"
msgstr "알림 소리를 바꿉니다"

#: src/gui/event-editor/gcal-alarm-row.ui:21
msgid "Remove the alarm"
msgstr "알림 제거"

#: src/gui/event-editor/gcal-event-editor-dialog.c:196
msgid "Save"
msgstr "저장"

#: src/gui/event-editor/gcal-event-editor-dialog.c:196
#: src/gui/event-editor/gcal-event-editor-dialog.ui:86
msgid "Done"
msgstr "완료"

#: src/gui/event-editor/gcal-event-editor-dialog.ui:29
msgid "Click to select the calendar"
msgstr "달력을 선택하려면 누르십시오"

#: src/gui/event-editor/gcal-event-editor-dialog.ui:115
msgid "Schedule"
msgstr "일정"

#: src/gui/event-editor/gcal-event-editor-dialog.ui:127
msgid "Reminders"
msgstr "알림"

#: src/gui/event-editor/gcal-event-editor-dialog.ui:139
msgid "Notes"
msgstr "참고"

#: src/gui/event-editor/gcal-event-editor-dialog.ui:159
msgid "Delete Event"
msgstr "행사 삭제"

#: src/gui/event-editor/gcal-reminders-section.ui:26
msgid "Add a Reminder…"
msgstr "알림 추가…"

#: src/gui/event-editor/gcal-reminders-section.ui:52
msgid "5 minutes"
msgstr "5분"

#: src/gui/event-editor/gcal-reminders-section.ui:58
msgid "10 minutes"
msgstr "10분"

#: src/gui/event-editor/gcal-reminders-section.ui:64
msgid "15 minutes"
msgstr "15분"

#: src/gui/event-editor/gcal-reminders-section.ui:70
msgid "30 minutes"
msgstr "30분"

#: src/gui/event-editor/gcal-reminders-section.ui:76
msgid "1 hour"
msgstr "1시간"

#: src/gui/event-editor/gcal-reminders-section.ui:82
msgid "1 day"
msgstr "1일"

#: src/gui/event-editor/gcal-reminders-section.ui:88
msgid "2 days"
msgstr "2일"

#: src/gui/event-editor/gcal-reminders-section.ui:94
msgid "3 days"
msgstr "3일"

#: src/gui/event-editor/gcal-reminders-section.ui:100
msgid "1 week"
msgstr "1주"

#. Translators: %A is the weekday name (e.g. Sunday, Monday, etc)
#: src/gui/event-editor/gcal-schedule-section.c:238
#, c-format
msgid "Last %A"
msgstr "최근 %A"

#: src/gui/event-editor/gcal-schedule-section.c:242
#: src/gui/gcal-event-popover.c:207 src/gui/gcal-event-popover.c:321
#: src/gui/views/gcal-agenda-view.c:186
msgid "Yesterday"
msgstr "어제"

#: src/gui/event-editor/gcal-schedule-section.c:246
#: src/gui/gcal-event-popover.c:199 src/gui/gcal-event-popover.c:313
#: src/gui/views/gcal-agenda-view.c:182
msgid "Today"
msgstr "오늘"

#: src/gui/event-editor/gcal-schedule-section.c:250
#: src/gui/gcal-event-popover.c:203 src/gui/gcal-event-popover.c:317
#: src/gui/views/gcal-agenda-view.c:184
msgid "Tomorrow"
msgstr "내일"

#. Translators: %A is the weekday name (e.g. Sunday, Monday, etc)
#: src/gui/event-editor/gcal-schedule-section.c:255
#, c-format
msgid "This %A"
msgstr "이번 %A"

#.
#. * Translators: %1$s is the formatted date (e.g. Today, Sunday, or even 2019-10-11) and %2$s is the
#. * formatted time (e.g. 03:14 PM, or 21:29)
#.
#: src/gui/event-editor/gcal-schedule-section.c:285
#, c-format
msgid "%1$s, %2$s"
msgstr "%1$s, %2$s"

#: src/gui/event-editor/gcal-schedule-section.ui:17
msgid "All Day"
msgstr "종일"

#: src/gui/event-editor/gcal-schedule-section.ui:34
#: src/gui/importer/gcal-import-file-row.c:152
msgid "Starts"
msgstr "시작"

#: src/gui/event-editor/gcal-schedule-section.ui:77
#: src/gui/importer/gcal-import-file-row.c:153
msgid "Ends"
msgstr "끝"

#: src/gui/event-editor/gcal-schedule-section.ui:120
msgid "Repeat"
msgstr "반복"

#: src/gui/event-editor/gcal-schedule-section.ui:128
msgid "No Repeat"
msgstr "반복 안함"

#: src/gui/event-editor/gcal-schedule-section.ui:129
msgid "Daily"
msgstr "매일"

#: src/gui/event-editor/gcal-schedule-section.ui:130
msgid "Monday – Friday"
msgstr "월요일 ~ 금요일"

#: src/gui/event-editor/gcal-schedule-section.ui:131
msgid "Weekly"
msgstr "주간"

#: src/gui/event-editor/gcal-schedule-section.ui:132
msgid "Monthly"
msgstr "월간"

#: src/gui/event-editor/gcal-schedule-section.ui:133
msgid "Yearly"
msgstr "연간"

#: src/gui/event-editor/gcal-schedule-section.ui:146
msgid "End Repeat"
msgstr "반복 끝"

#: src/gui/event-editor/gcal-schedule-section.ui:154
msgid "Forever"
msgstr "제한 없음"

#: src/gui/event-editor/gcal-schedule-section.ui:155
msgid "No. of occurrences"
msgstr "반복 횟수"

#: src/gui/event-editor/gcal-schedule-section.ui:156
msgid "Until Date"
msgstr "종료 일자"

#: src/gui/event-editor/gcal-schedule-section.ui:169
msgid "Number of Occurrences"
msgstr "반복 횟수"

#: src/gui/event-editor/gcal-schedule-section.ui:188
msgid "End Repeat Date"
msgstr "반복 종료일"

#: src/gui/event-editor/gcal-summary-section.c:78
#: src/gui/gcal-quick-add-popover.c:676
msgid "Unnamed event"
msgstr "이름 없는 행사"

#: src/gui/event-editor/gcal-summary-section.ui:16
#: src/gui/importer/gcal-import-file-row.c:150
msgid "Title"
msgstr "제목"

#: src/gui/event-editor/gcal-time-selector.ui:22
msgid ":"
msgstr ":"

#: src/gui/event-editor/gcal-time-selector.ui:46
#: src/gui/views/gcal-week-hour-bar.c:57
msgid "AM"
msgstr "오전"

#: src/gui/event-editor/gcal-time-selector.ui:47
#: src/gui/views/gcal-week-hour-bar.c:57
msgid "PM"
msgstr "오후"

#: src/gui/gcal-application.c:58
msgid "Quit GNOME Calendar"
msgstr "그놈 달력 끝내기"

#: src/gui/gcal-application.c:63
msgid "Display version number"
msgstr "버전 번호를 표시합니다"

#: src/gui/gcal-application.c:68
msgid "Enable debug messages"
msgstr "디버깅 메시지 출력 활성"

#: src/gui/gcal-application.c:73
msgid "Open calendar on the passed date"
msgstr "과거 달력 열기"

#: src/gui/gcal-application.c:78
msgid "Open calendar showing the passed event"
msgstr "과거 행사를 보여주는 달력을 엽니다"

#: src/gui/gcal-application.c:144
#, c-format
msgid "Copyright © 2012–%d The Calendar authors"
msgstr "Copyright © 2012–%d The Calendar authors"

#: src/gui/gcal-application.c:180
msgid "The GNOME Project"
msgstr "그놈 프로젝트"

#: src/gui/gcal-application.c:189
msgid "translator-credits"
msgstr "조성호 <shcho@gnome.org>"

#: src/gui/gcal-application.c:194 src/gui/gcal-window.ui:283
msgid "Weather"
msgstr "날씨"

#: src/gui/gcal-calendar-button.ui:6
msgid "Manage your calendars"
msgstr "달력을 관리합니다"

#: src/gui/gcal-calendar-button.ui:23
msgid "_Calendars"
msgstr "달력(_C)"

#: src/gui/gcal-calendar-button.ui:42
msgctxt "tooltip"
msgid "Synchronizing remote calendars…"
msgstr "원격 달력 동기화 중…"

#: src/gui/gcal-calendar-button.ui:72
msgid "_Synchronize Calendars"
msgstr "달력 동기화(_S)"

#: src/gui/gcal-calendar-button.ui:76
msgid "Manage Calendars…"
msgstr "달력 관리…"

#: src/gui/gcal-event-popover.c:122 src/gui/gcal-quick-add-popover.c:255
msgid "January"
msgstr "1월"

#: src/gui/gcal-event-popover.c:123 src/gui/gcal-quick-add-popover.c:256
msgid "February"
msgstr "2월"

#: src/gui/gcal-event-popover.c:124 src/gui/gcal-quick-add-popover.c:257
msgid "March"
msgstr "3월"

#: src/gui/gcal-event-popover.c:125 src/gui/gcal-quick-add-popover.c:258
msgid "April"
msgstr "4월"

#: src/gui/gcal-event-popover.c:126 src/gui/gcal-quick-add-popover.c:259
msgid "May"
msgstr "5월"

#: src/gui/gcal-event-popover.c:127 src/gui/gcal-quick-add-popover.c:260
msgid "June"
msgstr "6월"

#: src/gui/gcal-event-popover.c:128 src/gui/gcal-quick-add-popover.c:261
msgid "July"
msgstr "7월"

#: src/gui/gcal-event-popover.c:129 src/gui/gcal-quick-add-popover.c:262
msgid "August"
msgstr "8월"

#: src/gui/gcal-event-popover.c:130 src/gui/gcal-quick-add-popover.c:263
msgid "September"
msgstr "9월"

#: src/gui/gcal-event-popover.c:131 src/gui/gcal-quick-add-popover.c:264
msgid "October"
msgstr "10월"

#: src/gui/gcal-event-popover.c:132 src/gui/gcal-quick-add-popover.c:265
msgid "November"
msgstr "11월"

#: src/gui/gcal-event-popover.c:133 src/gui/gcal-quick-add-popover.c:266
msgid "December"
msgstr "12월"

#: src/gui/gcal-event-popover.c:158
#, c-format
msgid "Today %s"
msgstr "오늘 %s"

#: src/gui/gcal-event-popover.c:162
#, c-format
msgid "Tomorrow %s"
msgstr "내일 %s"

#: src/gui/gcal-event-popover.c:166
#, c-format
msgid "Yesterday %s"
msgstr "어제 %s"

#.
#. * Translators: %1$s is a month name (e.g. November), %2$d is the day
#. * of month, and %3$ is the hour. This format string results in dates
#. * like "November 21, 22:00".
#.
#: src/gui/gcal-event-popover.c:175
#, c-format
msgid "%1$s %2$d, %3$s"
msgstr "%1$s %2$d일, %3$s"

#.
#. * Translators: %1$s is a month name (e.g. November), %2$d is the day
#. * of month, %3$d is the year, and %4$s is the hour. This format string
#. * results in dates like "November 21, 2020, 22:00".
#.
#: src/gui/gcal-event-popover.c:187
#, c-format
msgid "%1$s %2$d, %3$d, %4$s"
msgstr "%3$d년 %1$s %2$d일, %4$s"

#.
#. * Translators: %1$s is a month name (e.g. November), and %2$d is
#. * the day of month. This format string results in dates like
#. * "November 21".
#.
#: src/gui/gcal-event-popover.c:216 src/gui/gcal-event-popover.c:330
#, c-format
msgid "%1$s %2$d"
msgstr "%1$s %2$d일"

#.
#. * Translators: %1$s is a month name (e.g. November), %2$d is the day
#. * of month, and %3$d is the year. This format string results in dates
#. * like "November 21, 2020".
#.
#: src/gui/gcal-event-popover.c:227 src/gui/gcal-event-popover.c:341
#, c-format
msgid "%1$s %2$d, %3$d"
msgstr "%3$d년 %1$s %2$d일"

#.
#. * Translators: %1$s is the start hour, and %2$s is the end hour, for
#. * example: "Today, 19:00 — 22:00"
#.
#: src/gui/gcal-event-popover.c:261
#, c-format
msgid "Today, %1$s — %2$s"
msgstr "오늘, %1$s – %2$s"

#.
#. * Translators: %1$s is the start hour, and %2$s is the end hour, for
#. * example: "Tomorrow, 19:00 — 22:00"
#.
#: src/gui/gcal-event-popover.c:269
#, c-format
msgid "Tomorrow, %1$s – %2$s"
msgstr "내일, %1$s – %2$s"

#.
#. * Translators: %1$s is the start hour, and %2$s is the end hour, for
#. * example: "Tomorrow, 19:00 — 22:00"
#.
#: src/gui/gcal-event-popover.c:277
#, c-format
msgid "Yesterday, %1$s – %2$s"
msgstr "어제, %1$s – %2$s"

#.
#. * Translators: %1$s is a month name (e.g. November), %2$d is the day
#. * of month, %3$s is the start hour, and %4$s is the end hour. This
#. * format string results in dates like "November 21, 19:00 — 22:00".
#.
#: src/gui/gcal-event-popover.c:286
#, c-format
msgid "%1$s %2$d, %3$s – %4$s"
msgstr "%1$s %2$d일, %3$s – %4$s"

#.
#. * Translators: %1$s is a month name (e.g. November), %2$d is the day
#. * of month, %3$d is the year, %4$s is the start hour, and %5$s is the
#. * end hour. This format string results in dates like:
#. *
#. * "November 21, 2021, 19:00 — 22:00".
#.
#: src/gui/gcal-event-popover.c:301
#, c-format
msgid "%1$s %2$d, %3$d, %4$s – %5$s"
msgstr "%3$d년 %1$s %2$d일, %4$s – %5$s"

#: src/gui/gcal-event-popover.ui:71
msgid "No event information"
msgstr "행사 정보 없음"

#: src/gui/gcal-event-popover.ui:169
msgid "Edit…"
msgstr "편집…"

#. Translators: %s is the location of the event (e.g. "Downtown, 3rd Avenue")
#: src/gui/gcal-event-widget.c:351
#, c-format
msgid "At %s"
msgstr "장소: %s"

#: src/gui/gcal-meeting-row.c:65
msgid "Google Meet"
msgstr "구글 미트"

# 자유 공개 소스 화상 회의 서비스.  https://jitsi.org
#: src/gui/gcal-meeting-row.c:66
msgid "Jitsi"
msgstr "짓시"

# 화상 회의 서비스.  https://whereby.com
#: src/gui/gcal-meeting-row.c:67
msgid "Whereby"
msgstr "웨어바이"

# 화상 회의 서비스.  https://zoom.com
#: src/gui/gcal-meeting-row.c:68
msgid "Zoom"
msgstr "줌"

#: src/gui/gcal-meeting-row.c:69
msgid "Microsoft Teams"
msgstr "마이크로소프트 팀즈"

#: src/gui/gcal-meeting-row.c:79
msgid "Unknown Service"
msgstr "알 수 없는 서비스"

#. Translators: "Join" as in "Join meeting"
#: src/gui/gcal-meeting-row.ui:12
msgid "Join"
msgstr "참석"

#: src/gui/gcal-quick-add-popover.c:118
#, c-format
msgid "%s (this calendar is read-only)"
msgstr "%s(읽기 전용 달력)"

#: src/gui/gcal-quick-add-popover.c:233
msgid "from next Monday"
msgstr "다음주 월요일부터"

#: src/gui/gcal-quick-add-popover.c:234
msgid "from next Tuesday"
msgstr "다음주 화요일부터"

#: src/gui/gcal-quick-add-popover.c:235
msgid "from next Wednesday"
msgstr "다음주 수요일부터"

#: src/gui/gcal-quick-add-popover.c:236
msgid "from next Thursday"
msgstr "다음주 목요일부터"

#: src/gui/gcal-quick-add-popover.c:237
msgid "from next Friday"
msgstr "다음주 금요일부터"

#: src/gui/gcal-quick-add-popover.c:238
msgid "from next Saturday"
msgstr "다음주 토요일부터"

#: src/gui/gcal-quick-add-popover.c:239
msgid "from next Sunday"
msgstr "다음주 일요일부터"

#: src/gui/gcal-quick-add-popover.c:244
msgid "to next Monday"
msgstr "다음주 월요일까지"

#: src/gui/gcal-quick-add-popover.c:245
msgid "to next Tuesday"
msgstr "다음주 화요일까지"

#: src/gui/gcal-quick-add-popover.c:246
msgid "to next Wednesday"
msgstr "다음주 수요일까지"

#: src/gui/gcal-quick-add-popover.c:247
msgid "to next Thursday"
msgstr "다음주 목요일까지"

#: src/gui/gcal-quick-add-popover.c:248
msgid "to next Friday"
msgstr "다음주 금요일까지"

#: src/gui/gcal-quick-add-popover.c:249
msgid "to next Saturday"
msgstr "다음주 토요일까지"

#: src/gui/gcal-quick-add-popover.c:250
msgid "to next Sunday"
msgstr "다음주 일요일까지"

#: src/gui/gcal-quick-add-popover.c:275
#, c-format
msgid "from Today"
msgstr "오늘부터"

#: src/gui/gcal-quick-add-popover.c:279
#, c-format
msgid "from Tomorrow"
msgstr "내일부터"

#: src/gui/gcal-quick-add-popover.c:283
#, c-format
msgid "from Yesterday"
msgstr "어제부터"

#. Translators:
#. * this is the format string for representing a date consisting of a month
#. * name and a date of month.
#.
#: src/gui/gcal-quick-add-popover.c:301
#, c-format
msgid "from %1$s %2$s"
msgstr "%1$s %2$s부터"

#: src/gui/gcal-quick-add-popover.c:312
#, c-format
msgid "to Today"
msgstr "오늘까지"

#: src/gui/gcal-quick-add-popover.c:316
#, c-format
msgid "to Tomorrow"
msgstr "내일까지"

#: src/gui/gcal-quick-add-popover.c:320
#, c-format
msgid "to Yesterday"
msgstr "어제까지"

#. Translators:
#. * this is the format string for representing a date consisting of a month
#. * name and a date of month.
#.
#: src/gui/gcal-quick-add-popover.c:338
#, c-format
msgid "to %1$s %2$s"
msgstr "%1$s %2$s 까지"

#. Translators: %1$s is the start date (e.g. "from Today") and %2$s is the end date (e.g. "to Tomorrow")
#: src/gui/gcal-quick-add-popover.c:345
#, c-format
msgid "New Event %1$s %2$s"
msgstr "%1$s %2$s의 새 행사"

#: src/gui/gcal-quick-add-popover.c:362
#, c-format
msgid "New Event Today"
msgstr "오늘의 새 행사"

#: src/gui/gcal-quick-add-popover.c:366
#, c-format
msgid "New Event Tomorrow"
msgstr "내일의 새 행사"

#: src/gui/gcal-quick-add-popover.c:370
#, c-format
msgid "New Event Yesterday"
msgstr "어제의 새 행사"

#: src/gui/gcal-quick-add-popover.c:376
msgid "New Event next Monday"
msgstr "다음주 월요일의 새 행사"

#: src/gui/gcal-quick-add-popover.c:377
msgid "New Event next Tuesday"
msgstr "다음주 화요일의 새 행사"

#: src/gui/gcal-quick-add-popover.c:378
msgid "New Event next Wednesday"
msgstr "다음주 수요일의 새 행사"

#: src/gui/gcal-quick-add-popover.c:379
msgid "New Event next Thursday"
msgstr "다음주 목요일의 새 행사"

#: src/gui/gcal-quick-add-popover.c:380
msgid "New Event next Friday"
msgstr "다음주 금요일의 새 행사"

#: src/gui/gcal-quick-add-popover.c:381
msgid "New Event next Saturday"
msgstr "다음주 토요일의 새 행사"

#: src/gui/gcal-quick-add-popover.c:382
msgid "New Event next Sunday"
msgstr "다음주 일요일의 새 행사"

#. Translators: %d is the numeric day of month
#: src/gui/gcal-quick-add-popover.c:394
#, c-format
msgid "New Event on January %d"
msgstr "1월 %d일의 새 행사"

#: src/gui/gcal-quick-add-popover.c:395
#, c-format
msgid "New Event on February %d"
msgstr "2월 %d일의 새 행사"

#: src/gui/gcal-quick-add-popover.c:396
#, c-format
msgid "New Event on March %d"
msgstr "3월 %d일의 새 행사"

#: src/gui/gcal-quick-add-popover.c:397
#, c-format
msgid "New Event on April %d"
msgstr "4월 %d일의 새 행사"

#: src/gui/gcal-quick-add-popover.c:398
#, c-format
msgid "New Event on May %d"
msgstr "5월 %d일의 새 행사"

#: src/gui/gcal-quick-add-popover.c:399
#, c-format
msgid "New Event on June %d"
msgstr "6월 %d일의 새 행사"

#: src/gui/gcal-quick-add-popover.c:400
#, c-format
msgid "New Event on July %d"
msgstr "7월 %d일의 새 행사"

#: src/gui/gcal-quick-add-popover.c:401
#, c-format
msgid "New Event on August %d"
msgstr "8월 %d일의 새 행사"

#: src/gui/gcal-quick-add-popover.c:402
#, c-format
msgid "New Event on September %d"
msgstr "9월 %d일의 새 행사"

#: src/gui/gcal-quick-add-popover.c:403
#, c-format
msgid "New Event on October %d"
msgstr "10월 %d일의 새 행사"

#: src/gui/gcal-quick-add-popover.c:404
#, c-format
msgid "New Event on November %d"
msgstr "11월 %d일의 새 행사"

#: src/gui/gcal-quick-add-popover.c:405
#, c-format
msgid "New Event on December %d"
msgstr "12월 %d일의 새 행사"

#: src/gui/gcal-quick-add-popover.ui:87
msgid "Edit Details…"
msgstr "세부내용 편집…"

#: src/gui/gcal-toolbar-end.ui:9
msgctxt "tooltip"
msgid "Search for events"
msgstr "행사 검색"

#: src/gui/gcal-toolbar-end.ui:21
msgctxt "tooltip"
msgid "Add a new event"
msgstr "새 행사 추가"

#: src/gui/gcal-weather-settings.ui:17
msgid "Show Weather"
msgstr "날씨 정보 표시"

#: src/gui/gcal-weather-settings.ui:34
msgid "Automatic Location"
msgstr "자동 위치 정보"

#: src/gui/gcal-window.c:713
msgid "Another event deleted"
msgstr "삭제한 다른 행사"

#: src/gui/gcal-window.c:713
msgid "Event deleted"
msgstr "삭제한 행사"

#: src/gui/gcal-window.c:715
msgid "_Undo"
msgstr "실행 취소(_U)"

#: src/gui/gcal-window.ui:117
msgid "Main Menu"
msgstr "주 메뉴"

#: src/gui/gcal-window.ui:156 src/gui/gcal-window.ui:189
msgid "_Today"
msgstr "오늘(_T)"

#: src/gui/gcal-window.ui:223
msgid "_Week"
msgstr "주(_W)"

#: src/gui/gcal-window.ui:238
msgid "_Month"
msgstr "월(_M)"

#: src/gui/gcal-window.ui:279
msgid "_Online Accounts…"
msgstr "온라인 계정(_O)…"

#: src/gui/gcal-window.ui:291
msgid "_Keyboard Shortcuts"
msgstr "키보드 바로 가기 키(_K)"

#: src/gui/gcal-window.ui:295
msgid "_About Calendar"
msgstr "달력 정보(_A)"

#: src/gui/gtk/help-overlay.ui:11
msgctxt "shortcut window"
msgid "General"
msgstr "일반"

#: src/gui/gtk/help-overlay.ui:14
msgctxt "shortcut window"
msgid "New event"
msgstr "새 행사"

#: src/gui/gtk/help-overlay.ui:20
msgctxt "shortcut window"
msgid "Close window"
msgstr "창 닫기"

#: src/gui/gtk/help-overlay.ui:26
msgctxt "shortcut window"
msgid "Search"
msgstr "검색"

#: src/gui/gtk/help-overlay.ui:32
msgctxt "shortcut window"
msgid "Show help"
msgstr "도움말 표시"

#: src/gui/gtk/help-overlay.ui:38
msgctxt "shortcut window"
msgid "Shortcuts"
msgstr "바로 가기"

#: src/gui/gtk/help-overlay.ui:46
msgctxt "shortcut window"
msgid "Navigation"
msgstr "탐색"

#: src/gui/gtk/help-overlay.ui:49
msgctxt "shortcut window"
msgid "Go back"
msgstr "뒤로 이동"

#: src/gui/gtk/help-overlay.ui:55
msgctxt "shortcut window"
msgid "Go forward"
msgstr "앞으로 이동"

#: src/gui/gtk/help-overlay.ui:61
msgctxt "shortcut window"
msgid "Show today"
msgstr "오늘 표시"

#: src/gui/gtk/help-overlay.ui:67
msgctxt "shortcut window"
msgid "Next view"
msgstr "다음 보기"

#: src/gui/gtk/help-overlay.ui:73
msgctxt "shortcut window"
msgid "Previous view"
msgstr "이전 보기"

#: src/gui/gtk/help-overlay.ui:81
msgctxt "shortcut window"
msgid "View"
msgstr "보기"

#: src/gui/gtk/help-overlay.ui:84
msgctxt "shortcut window"
msgid "Week view"
msgstr "주별 보기"

#: src/gui/gtk/help-overlay.ui:90
msgctxt "shortcut window"
msgid "Month view"
msgstr "월 단위 보기"

#: src/gui/importer/gcal-import-dialog.c:396
#, c-format
msgid "Import %d event"
msgid_plural "Import %d events"
msgstr[0] "행사 %d개 가져오기"

#: src/gui/importer/gcal-import-dialog.ui:4
msgid "Import Files…"
msgstr "파일 가져오기…"

#: src/gui/importer/gcal-import-dialog.ui:20 src/utils/gcal-utils.c:1320
msgid "_Cancel"
msgstr "취소(_C)"

#: src/gui/importer/gcal-import-dialog.ui:29
msgid "_Import"
msgstr "가져오기(_I)"

#: src/gui/importer/gcal-import-dialog.ui:50
msgid "C_alendar"
msgstr "달력(_A)"

#: src/gui/importer/gcal-importer.c:33
msgid "No error"
msgstr "오류 없음"

#: src/gui/importer/gcal-importer.c:36
msgid "Bad argument to function"
msgstr "동작 인자가 올바르지 않습니다"

#: src/gui/importer/gcal-importer.c:40
msgid "Failed to allocate a new object in memory"
msgstr "메모리로의 새 객체 할당에 실패했습니다"

#: src/gui/importer/gcal-importer.c:43
msgid "File is malformed, invalid, or corrupted"
msgstr "파일이 망가졌거나, 잘못되었거나, 깨졌습니다."

#: src/gui/importer/gcal-importer.c:46
msgid "Failed to parse the calendar contents"
msgstr "달력 내용 해석에 실패했습니다"

#: src/gui/importer/gcal-importer.c:49
msgid "Failed to read file"
msgstr "파일 읽기에 실패했습니다"

#: src/gui/importer/gcal-importer.c:56
msgid "Internal error"
msgstr "내부 오류"

#: src/gui/importer/gcal-importer.c:94
msgid "File is not an iCalendar (.ics) file"
msgstr "iCalendar(.ics) 파일이 아닙니다"

#: src/gui/importer/gcal-import-file-row.c:154
msgid "Description"
msgstr "설명"

#: src/gui/views/gcal-agenda-view.c:366
msgid "On-going"
msgstr "진행중"

#: src/gui/views/gcal-agenda-view.ui:19
msgid "No events"
msgstr "행사 없음"

#: src/gui/views/gcal-month-popover.ui:59
msgid "New Event…"
msgstr "새 행사…"

#: src/gui/views/gcal-week-grid.c:576
msgid "00 AM"
msgstr "오전 00"

#: src/gui/views/gcal-week-grid.c:579
msgid "00:00"
msgstr "00:00"

#: src/gui/views/gcal-week-header.c:472
#, c-format
msgid "Other event"
msgid_plural "Other %d events"
msgstr[0] "다른 행사 %d개"

#: src/gui/views/gcal-week-header.c:1000
#, c-format
msgid "week %d"
msgstr "%d주"

#: src/utils/gcal-utils.c:1317
msgid ""
"The event you are trying to modify is recurring. The changes you have "
"selected should be applied to:"
msgstr "수정하려는 행사가 반복됩니다. 다음 항목을 반영해야 합니다:"

#: src/utils/gcal-utils.c:1322
msgid "_Only This Event"
msgstr "이 행사에만(_O)"

#: src/utils/gcal-utils.c:1329
msgid "_Subsequent events"
msgstr "다음 행사(_S)"

#: src/utils/gcal-utils.c:1332
msgid "_All events"
msgstr "모든 행사(_A)"

#~ msgid "Year view"
#~ msgstr "연 단위 보기"

#~ msgid "Year"
#~ msgstr "연"

#~ msgctxt "shortcut window"
#~ msgid "Year view"
#~ msgstr "연 단위 보기"

#~ msgid "Follow system night light"
#~ msgstr "시스템 야간 모드 따르기"

#~ msgid "Use GNOME night light setting to activate night-mode."
#~ msgstr "그놈 야간 모드 설정에 따라 야간 모드를 설정합니다."

#~ msgid "%B %d…"
#~ msgstr "%B %d일…"

#~ msgid "%B %d"
#~ msgstr "%B %d일"

#~ msgid "Add Event…"
#~ msgstr "행사 추가…"

#~ msgid "Search for events"
#~ msgstr "행사 검색"

#~ msgid "Calendar management"
#~ msgstr "달력 관리자"

#~ msgid "Check this out!"
#~ msgstr "확인!"

#~ msgid "Date"
#~ msgstr "날짜"

#~ msgid "Time"
#~ msgstr "시간"

#~ msgid "From Web…"
#~ msgstr "웹에서 가져오기…"

#~ msgid "New Local Calendar…"
#~ msgstr "새 로컬 달력…"

#~ msgid "No results found"
#~ msgstr "결과가 없습니다"

#~ msgid "Try a different search"
#~ msgstr "다른 방법으로 검색 시도"

#~ msgid "%d week before"
#~ msgid_plural "%d weeks before"
#~ msgstr[0] "%d주 전"

#~ msgid "%s AM"
#~ msgstr "오전 %s"

#~ msgid "%s PM"
#~ msgstr "오후 %s"

#~ msgid "org.gnome.Calendar"
#~ msgstr "org.gnome.Calendar"

#~ msgid "Open online account settings"
#~ msgstr "온라인 계정 설정을 엽니다"

#~ msgid "Click to set up"
#~ msgstr "설정하려면 누르십시오"

#~ msgid "Nextcloud"
#~ msgstr "Nextcloud"

#~ msgid "Overview"
#~ msgstr "개요"

#~ msgid "Edit Calendar"
#~ msgstr "달력 편집"

#~ msgid "Calendar Address"
#~ msgstr "달력 주소"

#~ msgid "All day"
#~ msgstr "종일"

#~ msgid "Use the entry above to search for events."
#~ msgstr "행사를 검색하려면 상단의 항목을 사용하십시오."

#~ msgid "Unnamed Calendar"
#~ msgstr "이름 없는 달력"

#~ msgid "Off"
#~ msgstr "끔"

#~ msgid "On"
#~ msgstr "켬"

#~ msgid "Expired"
#~ msgstr "기한 지남"

#~ msgid "_About"
#~ msgstr "정보(_A)"

#~ msgid "_Quit"
#~ msgstr "끝내기(_Q)"

#~ msgid "Add Eve_nt…"
#~ msgstr "행사 추가(_N)…"

#~ msgid "Add Eve_nt"
#~ msgstr "행사 추가(_N)"

#~ msgid "— Calendar management"
#~ msgstr "— 달력 관리"

#~ msgid "Copyright © %d The Calendar authors"
#~ msgstr "Copyright © %d The Calendar authors"

#~ msgid "Other events"
#~ msgstr "다른 행사"

#~ msgid "List of the disabled sources"
#~ msgstr "비활성화 원본 검색"

#~ msgid "Sources disabled last time Calendar ran"
#~ msgstr "최근 달력을 실행한 후 사용하지 않기로 설정한 원본입니다"

#~ msgid "_Search…"
#~ msgstr "검색(_S)…"

#~ msgid "_Calendars…"
#~ msgstr "달력(_C)…"

#~ msgid "week %d / %d"
#~ msgstr "%d / %d 주"

#~ msgid "New Event from %s to %s"
#~ msgstr "%s부터 %s까지의 새 행사"

#~ msgid "New Event on %s, %s – %s"
#~ msgstr "%2$s부터 %3$s까지, %1$s의 새 행사"

#~ msgid "Midnight"
#~ msgstr "자정"

#~ msgid "Noon"
#~ msgstr "정오"

#~ msgid "00:00 PM"
#~ msgstr "오후 00:00"

#~ msgid "Create"
#~ msgstr "만들기"

#~ msgid "%m/%d/%y"
#~ msgstr "%y/%m/%d"

#~ msgid "%.2d:%.2d AM"
#~ msgstr "오전 %.2d:%.2d"

#~ msgid "%.2d:%.2d PM"
#~ msgstr "오후 %.2d:%.2d"

#~ msgid "More Details"
#~ msgstr "더 자세히"

#~ msgid "%.2d:%.2d %s"
#~ msgstr "%3$s %1$.2d:%2$.2d"

#~ msgid "Unable to initialize GtkClutter"
#~ msgstr "GtkClutter를 초기화 할 수 없습니다"

#~ msgid "Weeks"
#~ msgstr "주별"

#~ msgid "Months"
#~ msgstr "월별"

#~ msgid "Years"
#~ msgstr "연도별"

#~ msgid "Change the date"
#~ msgstr "날짜 바꾸기"

#~ msgid "What (e.g. Alien Invasion)"
#~ msgstr "할 일 (예: 외계인 침공)"

#~ msgid "Email"
#~ msgstr "전자메일"

#~ msgid "WWW 99 - WWW 99"
#~ msgstr "WWW 99 - WWW 99"

#~ msgid "Change the time"
#~ msgstr "시간 바꾸기"

#~ msgid "List"
#~ msgstr "목록"

#~ msgid "Back"
#~ msgstr "뒤로"

#~ msgid "Year %d"
#~ msgstr "%d년"
